from PIL import Image
import os, random

common_path = "./icons/padded/common"
reserved_path = "./icons/padded/reserved"
cards_path = "./cards"

frame_width = 6
frame = Image.new("RGBA", (512,512), color="#000000ff")
frame_inner = Image.new("RGBA", (512-frame_width,512-frame_width), color="#00000000")

frame.paste(frame_inner,(int(frame_width/2),int(frame_width/2)))

def getFileList(path):
    filenames= os.listdir(path) # get all files' and folders' names in the current directory

    result = []
    for filename in filenames: # loop through all the files and folders
        file = os.path.join(path, filename)
        if not os.path.isdir(file): # check whether the current object is a folder or not
            result.append(file)

    print(result)
    return result

def loadImages(files):
    result = []

    for file in files:
        im = Image.open(file)
        
        im.paste(frame,(0,0), frame)
    

        result.append(im)


    return result

def merge_images(file1, file2):
    """Merge two images into one, displayed side by side
    :param file1: path to first image file
    :param file2: path to second image file
    :return: the merged Image object
    """
    image1 = Image.open(file1)
    image2 = Image.open(file2)

    (width1, height1) = image1.size
    (width2, height2) = image2.size

    result_width = width1 + width2
    result_height = max(height1, height2)

    result = Image.new('RGB', (result_width, result_height))
    result.paste(im=image1, box=(0, 0))
    result.paste(im=image2, box=(width1, 0))
    return result


if __name__=="__main__":
    common_files = getFileList(common_path)
    reserved_files = getFileList(reserved_path)

    common_images = loadImages(common_files)
    reserved_images = loadImages(reserved_files)

    blank_grid = [
        [0,0,0,0,1,],
        [0,1,0,0,0,],
        [1,0,0,0,0,],
        [0,0,0,1,0,],
        [0,0,1,0,0,],
    ]

    cards = 30
    random.seed(1024)

    for cardnum in range(cards):
        grid = blank_grid.copy()
        c_imgs = common_images.copy()
        random.shuffle(c_imgs)
        r_imgs = reserved_images.copy()
        random.shuffle(r_imgs)

        result = Image.new('RGB', (5*512, 5*512), color = "#ffffff")


        for x in range(len(grid)):
            for y in range(len(grid[x])):
                cell = grid[x][y]
                if cell == 0:
                    im = c_imgs.pop(0)
                    result.paste(im=im, box=(512*x, 512*y), mask=im)
                else:
                    im = r_imgs.pop(0)
                    result.paste(im=im, box=(512*x, 512*y), mask=im)

        result.save(os.path.join(cards_path, "card_%d.png" % cardnum ))
        #result.show()
        #input()


