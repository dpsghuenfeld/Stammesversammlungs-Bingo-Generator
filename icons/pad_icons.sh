
# scale and pad horizontal

for i in original/*.png;
  do name=`echo "$i" | cut -d'.' -f1`
  echo "$name"
  ffmpeg -i "$i" -vf "scale=-1:512,pad=512:512:x=(ow-iw)/2:color=#00000000" -y "padded/${i}"
done


#scale and pad vertical

for i in original/*.png;
  do name=`echo "$i" | cut -d'.' -f1`
  echo "$name"
  ffmpeg -i "$i" -vf "scale=512:-1,pad=512:512:y=(oh-ih)/2:color=#00000000" -y "padded/${i}"
done
